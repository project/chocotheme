
WHAT IS CHOCOTheme?
------------------------

CHOCOTheme is a theme with div based, fixed width,
two column layout (content+right sidebar).


INSTALLATION
------------

 1. Download CHOCOTheme from http://drupal.org/project/chocotheme

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configurationl
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Full documentation on using CHOCOTheme:
  http://www.freedrupalthemes.net/docs/chocotheme

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
  
CHOCOTheme demo site
  http://d7.freedrupalthemes.net/t/chocotheme
